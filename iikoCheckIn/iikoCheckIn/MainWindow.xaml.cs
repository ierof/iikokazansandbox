﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;
using iikoCheckIn.Properties;

namespace iikoCheckIn
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ViewModel { ColCount = Settings.Default.ColCount };
            FillDataSafe();
        }

        const string url = @"https://script.google.com/macros/s/AKfycbwjIQzVFsAi8jCZ1z-CP3wQGCLa7AhpEvEGNRzQ7CcLuoIpuI_d/exec";
        private readonly Uri uri = new Uri(url);
        private readonly Encoding encoding = Encoding.UTF8;

        private void FillDataSafe()
        {
            try
            {
                FillData();
            }
            catch
            {
                MessageBox.Show("Ошибка при загрузке данных");
            }
        }

        private void FillData()
        {
            string downloadString;
            using (var client = new WebClient())
            {
                client.Encoding = encoding;
                downloadString = client.DownloadString(uri);
            }

            string[] separatorInWebApp =
            {
                "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
                "----------------------------------------------------------------"
            };
            var data = downloadString.Split(separatorInWebApp, StringSplitOptions.None)[1];
            var persons = Storage.Read();

            lstUsers.Items.Clear();
            var ids = new HashSet<Guid>();
            foreach (var name in data.Split(new[] { "\\\\n" }, StringSplitOptions.None))
            {
                if (name.Length > 1 && name != "undefined")
                {
                    var person = persons.Persons.FirstOrDefault(p => p.Name == name);
                    if (person == null)
                    {
                        person = new Person { Id = Guid.NewGuid(), IsHere = false, Name = name };
                        persons.Persons.Add(person);
                    }

                    ids.Add(person.Id);
                    lstUsers.Items.Add(new UiPerson(name) { LastSeen = person.LastSeen, IsChecked = person.IsHere, SyncTime = !person.SkipUploadData });
                }
            }

            persons.Persons.RemoveAll(p => !ids.Contains(p.Id));
            Storage.Write(persons);
        }

        private void PersonButton_Click(object sender, RoutedEventArgs e)
        {
            var uiPerson = GetUiPerson(sender);
            if (uiPerson == null)
                return;
            uiPerson.LastSeen = DateTime.Now;
            var storage = Storage.Read();
            var person = storage.Persons.FirstOrDefault(p => p.Name == uiPerson.Name);
            if (person == null)
                return;
            person.LastSeen = uiPerson.LastSeen;
            uiPerson.IsChecked = !uiPerson.IsChecked;
            person.IsHere = uiPerson.IsChecked;

            Storage.Write(storage);
            if (person.SkipUploadData)
                return;

            var toSend = new NameValueCollection
            {
                {"name", person.Name},
                {"date", person.LastSeen.Value.Day.ToString()},
                {"time", person.LastSeen.Value.TimeOfDay.ToString(@"hh\:mm")},
                /* ушёл ли человек*/
                {"left", (!person.IsHere).ToString()}
            };

            using (var client = new WebClient())
            {
                client.Encoding = encoding;
                client.UploadValuesAsync(uri, toSend);
            }
        }

        private static UiPerson GetUiPerson(object sender)
        {
            var btn = sender as ButtonBase;
            return btn?.DataContext as UiPerson;
        }

        private void PersonSyncTimeButton_Click(object sender, RoutedEventArgs e)
        {
            var uiPerson = GetUiPerson(sender);
            if (uiPerson == null)
                return;
            var storage = Storage.Read();
            var person = storage.Persons.FirstOrDefault(p => p.Name == uiPerson.Name);
            if (person == null)
                return;
            e.Handled = true;
            var text = person.SkipUploadData
                ? "Начать выгружать время прихода/ухода в Табель?"
                : "Прекратить выгружать время прихода/ухода в Табель?";
            if (MessageBox.Show(text, "Выгрузка", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;
            person.SkipUploadData = !person.SkipUploadData;
            uiPerson.SyncTime = !person.SkipUploadData;
            Storage.Write(storage);
        }
    }

    internal class UiPerson : DependencyObject
    {
        public UiPerson(string name)
        {
            Name = name;
        }
        public string Name { get; }

        public static readonly DependencyProperty LastSeenProperty =
            DependencyProperty.Register("LastSeen", typeof(DateTime?), typeof(UiPerson), new UIPropertyMetadata(null));
        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.Register("IsChecked", typeof(bool), typeof(UiPerson), new UIPropertyMetadata(null));
        public static readonly DependencyProperty SyncTimeProperty =
            DependencyProperty.Register("SyncTime", typeof(bool), typeof(UiPerson), new UIPropertyMetadata(null));
        public DateTime? LastSeen
        {
            get { return (DateTime?)GetValue(LastSeenProperty); }
            set { SetValue(LastSeenProperty, value); }
        }

        public bool IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }
        public bool SyncTime
        {
            get { return (bool)GetValue(SyncTimeProperty); }
            set { SetValue(SyncTimeProperty, value); }
        }
    }

    internal class ViewModel
    {
        public int ColCount { get; set; }
    }

}
