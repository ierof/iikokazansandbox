﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace iikoCheckIn
{

    [Serializable]
    public class Storage
    {
        private const string path = "data.dat";

        public static Storage Read()
        {
            if (!File.Exists(path))
            {
                var result = new Storage { Persons = new List<Person>() };
                Write(result);
                return result;
            }
            using (StreamReader file = new StreamReader(path))
            {
                var result = JsonConvert.DeserializeObject<Storage>(file.ReadToEnd());
                file.Close();
                return result;
            }
        }

        public static void Write(object myObj)
        {
            using (StreamWriter myFile = new StreamWriter(path, false))
            {
                myFile.WriteLine(JsonConvert.SerializeObject(myObj));
                myFile.Close();
            }
        }
        public List<Person> Persons { get; set; }
    }

    [Serializable]
    public class Person
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime? LastSeen { get; set; }
        public bool IsHere { get; set; }
        public bool SkipUploadData { get; set; }
    }
}
